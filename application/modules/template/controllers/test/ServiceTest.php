<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/template/libraries/ServiceClass.php';
include_once APPPATH . 'modules/template/controllers/test/builder/ServiceDataBuilder.php';

class ServiceTest extends Toast{
    private $builder;
    private $service;

    function __construct(){
        parent::__construct('ServiceTest');
    }

    function _pre(){
        $this->builder = new ServiceDataBuilder();
        $this->service = new ServiceClass();
    }

    function test_objetos_criados_corretamente(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->service, "Erro na criação da service");
    }

    function test_selecionado_banco_de_teste(){
        $s = $this->builder->database();
        $this->_assert_equals('lp2_modulo', $s, 'Erro na seleção do banco de teste');
    }

    function test_insere_registro_na_tabela(){
        // cenário 1: vetor com dados corretos
        $this->builder->clean_table();
        $data = $this->builder->getData(0);
        
        $id1 = $this->service->insert($data, 'service');
        $this->_assert_equals(1, $id1, "Esperado 1, recebido $id1");

        // verificação: o objeto criado é, de fato, aquele que enviamos?
        $task = $this->service->get(array('id' => 1), 'service')[0];
        $this->_assert_equals($data['title'], $task['title']);
        $this->_assert_equals($data['text'], $task['text']);
        $this->_assert_equals($data['price'], $task['price']);


        // cenário 2: vetor com dados inesperados
        $info = $this->builder->getData(1);
        $info['unexpected_col_name'] = 1;
        $id = $this->service->insert($info);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");

        // cenário 3: vetor com dados incompletos... deve ser
        // tratado pela validação, mas tem que ser pensado aqui
        $v = array('titulo' => 'vetor incompleto');
        $id = $this->service->insert($v);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");
    }

    function test_carrega_todos_os_registros_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $tasks = $this->service->get();
        $this->_assert_equals(3, sizeof($tasks), "Número de registros incorreto");
    }

    function test_carrega_registro_condicionalmente(){
        $this->builder->clean_table();
        $this->builder->build();

        $task = $this->service->get(array('price' => 4000))[0];
        $this->_assert_equals(4000, $task['price'], "Erro no preço");
    }
    
    function test_atualiza_registro(){
        $this->builder->clean_table();
        $this->builder->build();

        // lê um registro do bd
        $task1 = $this->service->get(array('id' => 2))[0];
        $this->_assert_equals(12000, $task1['price'], "Erro no preço");

        // atualiza seus valores
        $task1['price'] = 8000;
        $this->service->insert_or_update($task1);

        // lê novamente, o mesmo objeto, e verifica se foi atualizado
        $task2 = $this->service->get(array('id' => 2))[0];
        $this->_assert_equals($task1['price'], $task2['price'], "Erro no preço");
    }
    
    function test_remove_registro_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        // verifica que o registro existe
        $task1 = $this->service->get(array('id' => 2))[0];
        $this->_assert_equals(12000, $task1['price'], "Erro no preço");

        // remove o registro
        $this->service->delete(array('id' => 2));

        // verifica que o registro não existe mais
        $task2 = $this->service->get(array('id' => 2));
        $this->_assert_equals_strict(0, sizeof($task2), "Registro não foi removido");
    }

}