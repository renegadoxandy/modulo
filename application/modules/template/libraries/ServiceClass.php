<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceClass extends Dao {

    function __construct(){
        parent::__construct('service');
    }

    public function insert($data, $table = 'null') {
        // mais uma camada de segurança... além da validação
        
        $cols = array('title', 'text', 'price');
        $this->expected_cols($cols);

        return parent::insert($data, $table);
    }
}