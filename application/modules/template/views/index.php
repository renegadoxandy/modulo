<section class="jumbotron" style="min-height:650px;margin:0px" id="quemSomos">
    <div class="container">
        <h1 class="jumbotron-heading">Serviços/Produtos</h1><br>
        <div class="container mt-3">
            <form method="post" action="<?= $action ?>">
                <div class="form-group">
                    <label for="exampleFormControlInput1" >Título</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" name="title" value="<?php if(isset($title)) echo $title ?>" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput2">Texto</label>
                    <input type="text" class="form-control" id="exampleFormControlInput2" name="text" value="<?php if(isset($text)) echo $text ?>" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput3">Valor</label>
                    <input type="number" class="form-control" id="exampleFormControlInput3" name="price" value="<?php if(isset($price)) echo $price ?>" required>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</section>