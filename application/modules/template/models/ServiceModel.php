<?php

class ServiceModel extends CI_Model {

    public function getDataForm(){
        $data['title'] = $this->input->post('title');
        $data['text'] = $this->input->post('text');
        $data['price'] = $this->input->post('price');
        return $data;
    }
    public function store(){
        $data = $this->getDataForm();
        $this->db->insert('service', $data);
    }
    public function destroy($id){
        $this->db->where('id', $id)->delete('service');
    }
    public function update($id){
        $data = $this->getDataForm();
        $this->db->where('id', $id);
        $this->db->update('service', $data);
    }
    public function selectAll(){
        return $this->db->get('service')->result_array();
    }
    public function select($id){
        return $this->db->where('id', $id)->get('service')->result_array()[0];
    }
}