<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class ServiceDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_modulo'){
        parent::__construct('service', $table);
    }

    function getData($index = -1){
        $data[0]['title'] = 'Celular';
        $data[0]['text'] = 'Celular de ultima geração';
        $data[0]['price'] = 4000;

        $data[1]['title'] = 'Televisão';
        $data[1]['text'] = 'Televisão de ultima geração';
        $data[1]['price'] = 12000;

        $data[2]['title'] = 'Microondas';
        $data[2]['text'] = 'Apenas um microondas qualquer';
        $data[2]['price'] = 500;

        return $index > -1 ? $data[$index] : $data;
    }

}